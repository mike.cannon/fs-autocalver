const dateFormat = require('dateformat')
const readJson = require('read-package-json')
const writePackage = require('write-pkg')

var d = new Date()

var dateString = dateFormat(d, "yyyy.mm.dd")

var version = 1

readJson(`${process.cwd()}/package.json`, (er, data) => {

    if (er) {
        console.error("There was an error reading the file")
        console.log(er)
        return
      }

    json = data

    // pull existing calver (if exists)
    if (json.calver && json.calver.includes(dateString)) {
        version = Number(json.calver.substring(11, json.calver.length))
        version++
    }

    json.calver = dateString.concat('.' + version)

    // overwrite existing value
    writePackage(process.cwd(), json);
    console.log('done');

});